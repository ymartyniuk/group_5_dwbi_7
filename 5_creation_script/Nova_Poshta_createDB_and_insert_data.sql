﻿USE master;
GO

DROP DATABASE IF EXISTS Nova_Poshta;
GO

CREATE DATABASE Nova_Poshta;
GO

USE Nova_Poshta;
GO

DROP TABLE IF EXISTS warehouse;
DROP TABLE IF EXISTS schedule;
DROP TABLE IF EXISTS client;
DROP TABLE IF EXISTS bank_account;
DROP TABLE IF EXISTS bank;
DROP TABLE IF EXISTS [order];
DROP TABLE IF EXISTS [city];
DROP TABLE IF EXISTS [city_region];
DROP TABLE IF EXISTS [city_district];
DROP TABLE IF EXISTS [country];
DROP TABLE IF EXISTS employee;
DROP TABLE IF EXISTS carrier;
DROP TABLE IF EXISTS customer_type;
DROP TABLE IF EXISTS shipping_point;
DROP TABLE IF EXISTS transport_area;
DROP TABLE IF EXISTS calculation_scheme;
DROP TABLE IF EXISTS deadline_type;
DROP TABLE IF EXISTS packing_types;
DROP TABLE IF EXISTS shipping_types;
DROP TABLE IF EXISTS shipment_type;
go

create table transport_area	(
transport_area_id	int identity primary key not null,
transport_area_text	varchar(40) default 'Пересічна дорога' not null
);
go

CREATE TABLE [city]
			([city_id] INT NOT NULL PRIMARY KEY,
			 [city_text] VARCHAR(40) NOT NULL DEFAULT ('Львів'),
			 [city_region_id] INT NOT NULL  DEFAULT (1),
			 [city_district_id] INT NOT NULL DEFAULT (1),
			 [country_id] INT NOT NULL  DEFAULT (1),
			 [transport_area_id] INT NULL DEFAULT (1),
			 [city_before_decomunization] VARCHAR(40) NULL
			 );
GO

CREATE TABLE [city_region]
			([id] INT NOT NULL PRIMARY KEY,
			 [city_region] VARCHAR(40) NOT NULL DEFAULT ('Львівська'),
			 );
GO

CREATE TABLE [city_district]
			([id] INT NOT NULL PRIMARY KEY,
			 [city_district] VARCHAR(40) NOT NULL
			 );
GO

CREATE TABLE [country]
			([id] INT NOT NULL PRIMARY KEY,
			 [country] VARCHAR(40) NOT NULL DEFAULT ('Україна'),
			 );
GO

ALTER TABLE [city]
ADD CONSTRAINT FK_city_city_region
FOREIGN KEY([city_region_id]) REFERENCES [city_region]([id])
ON DELETE CASCADE
ON UPDATE CASCADE;
GO

ALTER TABLE [city]
ADD CONSTRAINT FK_city_city_district
FOREIGN KEY([city_district_id]) REFERENCES [city_district]([id])
ON DELETE SET DEFAULT
ON UPDATE SET DEFAULT;
GO

ALTER TABLE [city]
ADD CONSTRAINT FK_city_country
FOREIGN KEY([country_id]) REFERENCES [country]([id])
ON DELETE CASCADE
ON UPDATE CASCADE;
GO

ALTER TABLE [city]
ADD CONSTRAINT FK_city_transport_area
FOREIGN KEY([transport_area_id]) REFERENCES [transport_area]([transport_area_id])
ON DELETE SET DEFAULT
ON UPDATE SET DEFAULT;
GO


create table carrier(
id int not null primary key,
mark_of_car varchar(30) not null,
name_of_driver varchar(30) not null,
surname_of_driver varchar(30) not null,
id_receiver_sender int not null,
[from] varchar(30) not null,
[to] varchar(30) not null,
departure_date date not null,
date_of_receiving date not null
);
go

create table employee(
id int not null primary key,
name varchar(30) not null,
surname varchar(30) not null,
patronymic varchar(30) null,
phone char(10) not null unique,
address varchar(50) not null,
date_of_hire date not null,
date_if_fire date null,
salary decimal(8,2) not null,
date_of_birth date not null
);
go

CREATE TABLE warehouse
(id INT NOT NULL PRIMARY KEY,
 city_id INT NOT NULL,
 number_within_city SMALLINT NOT NULL,
 street_name VARCHAR(50) NOT NULL,
 street_number VARCHAR(10) NOT NULL,
 postal_code CHAR(5) NOT NULL,
 location_cordinate CHAR(15) NOT NULL UNIQUE,
 telephone_number CHAR(13) NOT NULL UNIQUE,
 email VARCHAR(50) NULL,
 open_date DATE NOT NULL,
 close_date DATE NULL,
 head_id int NULL,
 warehouse_type VARCHAR(20) NOT NULL,
 weight_limitation SMALLINT NULL,
 office_area NUMERIC(5,2) NOT NULL,
 workstation_number TINYINT NOT NULL,
 monthly_rent NUMERIC (7,2) NULL
);
go

CREATE TABLE schedule
(warehouse_id INT NOT NULL,
 [weekday] TINYINT NOT NULL,
 schedule_type VARCHAR(20) NOT NULL,
 open_time TIME NOT NULL,
 close_time TIME NOT NULL
 CONSTRAINT PK_schedule PRIMARY KEY(warehouse_id,[weekday],schedule_type)
);
go

CREATE TABLE client
(id INT NOT NULL PRIMARY KEY,
 city_id INT NOT NULL,
 street_name VARCHAR(50) NOT NULL,
 street_number VARCHAR(10) NOT NULL,
 client_type VARCHAR(20) NOT NULL,
 name VARCHAR(20) NOT NULL,
 last_name VARCHAR(30) NULL,
 middle_name VARCHAR(25) NULL,
 inn_code CHAR(10) NULL,
 passport CHAR(8) NULL,
 ownership_type VARCHAR(20) NULL,
 edrpou_code CHAR(8) NULL,
 telephone_number CHAR(13) NOT NULL UNIQUE,
 email VARCHAR(50) NULL
);
go

CREATE TABLE bank_account
(client_id INT NOT NULL,
 bank_id CHAR(6) NOT NULL,
 number VARCHAR(13) NOT NULL
 CONSTRAINT PK_bank_account PRIMARY KEY(client_id,bank_id,number)
);
go

CREATE TABLE bank
(id CHAR(6) NOT NULL PRIMARY KEY,
 edrpou_code CHAR(8) NOT NULL,
 full_name VARCHAR(50) NOT NULL
);
go


ALTER TABLE warehouse
ADD CONSTRAINT UK_warehouse
UNIQUE(city_id,number_within_city);

ALTER TABLE schedule
ADD CONSTRAINT FK_schedule_warehouse
FOREIGN KEY(warehouse_id)
REFERENCES warehouse(id);

ALTER TABLE warehouse
ADD CONSTRAINT FK_warehouse_city
FOREIGN KEY(city_id)
REFERENCES city(city_id);

ALTER TABLE warehouse
ADD CONSTRAINT FK_warehouse_head
FOREIGN KEY(head_id)
REFERENCES employee(id);

ALTER TABLE client
ADD CONSTRAINT FK_client_city
FOREIGN KEY(city_id)
REFERENCES city(city_id);
GO

create table customer_type(
customer_type_id int identity primary key not null,
customer_type_text	varchar(40) default 'ФОП' not null
);
go

create table shipping_point(
shipping_point_id	int identity primary key not null,
shipping_point_text	varchar(40) default 'Склад-Склад' not null
);
go

create table calculation_scheme	(
calculation_scheme_id	int identity primary key not null,
calculation_scheme_text	varchar(40) not null,
calculation_scheme_price decimal(13,5) not null,
calculation_scheme_discount_abs	decimal(13,5),
calculation_scheme_discount_perc decimal(13,5),
calculation_scheme_tax decimal (13,5) not null
);
go

create table deadline_type(
deadline_type_id int identity primary key not null,
deadline_type_text	varchar(40) not null,
deadline_type_delay	int default 0 check(deadline_type_delay>=0) not null
);
go

create table packing_types(
packing_id int identity primary key not null,
packing_text varchar(40) default 'Бандероль' not null,
packing_material varchar(40)
);
go

create table shipping_types	(
shipping_type_id int identity primary key not null,
shipping_type_text	varchar(40) default 'Автотранспорт' not null
);
go

create table shipment_type(
shipment_type_id int identity primary key not null,
shipment_type_text	varchar(40),
shipping_type_id int foreign key references shipping_types(shipping_type_id) not null,
base_unit varchar(40) default 'КГ',
base_currency varchar(40) default 'ГРН',
packing_id int foreign key references packing_types(packing_id) not null,
spec_packing varchar(40) default 'Без спеціальног пакування',
deadline_type_id int foreign key references deadline_type(deadline_type_id) not null,
calculation_scheme_id int foreign key references calculation_scheme(calculation_scheme_id) not null,
transport_area_id int foreign key references transport_area(transport_area_id) not null,
priority_grade	tinyint default 0,
shipping_point_id int foreign key references shipping_point(shipping_point_id) not null,
customer_type_id int foreign key references customer_type(customer_type_id) not null,
relevant_for_loyalty tinyint,
prepayment	tinyint not null
);
go

CREATE TABLE [order]
			([id] INT NOT NULL PRIMARY KEY,
			 [shipment_type_id] INT NOT NULL,
			 [tracking_number] INT NOT NULL UNIQUE,
			 [departure_warehouse_id] INT NOT NULL,
			 [destination_warehouse_id] INT NOT NULL,
			 [sender_id] INT NOT NULL,
			 [reciever_id] INT NOT NULL,
			 [payer_id] INT NOT NULL ,
			 [took_order_employee_id] INT NOT NULL,
			 [gave_out_order_employee_id] INT NULL,
			 [carrier_id] INT NULL,
			 [receiving_date] DATETIME NOT NULL,
			 [arriving_date] DATETIME NOT NULL,
			 [gave_out_date] DATETIME NULL,
			 [order_cost] MONEY NOT NULL DEFAULT (0) CHECK ([order_cost] >= 0),
			 [weight] real NOT NULL DEFAULT (0) CHECK ([weight] >= 0),
			 [total_cost] AS ([order_cost]+ISNULL(0.01*[order_cost]*DATEDIFF(DAY, [arriving_date],[gave_out_date]),0)),
			 [is_reversed] BIT NOT NULL,
			);
GO

ALTER TABLE [order]
ADD CONSTRAINT CK_order_arriving_date
CHECK ([arriving_date] >= [receiving_date]);
GO

ALTER TABLE [order]
ADD CONSTRAINT CK_order_gave_out_date
CHECK ([gave_out_date] >= [arriving_date]);
GO

ALTER TABLE [order]
ADD CONSTRAINT CK_order_payer
CHECK ([payer_id] in ([sender_id],[reciever_id]));
GO

ALTER TABLE [order]
ADD CONSTRAINT FK_order_shipment_type
FOREIGN KEY([shipment_type_id]) REFERENCES [shipment_type]([shipment_type_id])
ON DELETE NO ACTION
ON UPDATE NO ACTION;
GO

ALTER TABLE [order]
ADD CONSTRAINT FK_order_departure_warehouse
FOREIGN KEY([departure_warehouse_id]) REFERENCES [warehouse]([id])
ON DELETE NO ACTION
ON UPDATE NO ACTION;
GO

ALTER TABLE [order]
ADD CONSTRAINT FK_order_destination_warehouse
FOREIGN KEY([destination_warehouse_id]) REFERENCES [warehouse]([id])
ON DELETE NO ACTION
ON UPDATE NO ACTION;
GO

ALTER TABLE [order]
ADD CONSTRAINT FK_order_sender
FOREIGN KEY([sender_id]) REFERENCES [client]([id])
ON DELETE NO ACTION
ON UPDATE NO ACTION;
GO

ALTER TABLE [order]
ADD CONSTRAINT FK_order_reciever
FOREIGN KEY([reciever_id]) REFERENCES [client]([id])
ON DELETE NO ACTION
ON UPDATE NO ACTION;
GO

ALTER TABLE [order]
ADD CONSTRAINT FK_order_payer
FOREIGN KEY([payer_id]) REFERENCES [client]([id])
ON DELETE NO ACTION
ON UPDATE NO ACTION;
GO

ALTER TABLE [order]
ADD CONSTRAINT FK_order_took_order_employee
FOREIGN KEY([took_order_employee_id]) REFERENCES [employee]([id])
ON DELETE NO ACTION
ON UPDATE NO ACTION;
GO

ALTER TABLE [order]
ADD CONSTRAINT FK_order_gave_out_order_employee
FOREIGN KEY([gave_out_order_employee_id]) REFERENCES [employee]([id])
ON DELETE NO ACTION
ON UPDATE NO ACTION;
GO

ALTER TABLE [order]
ADD CONSTRAINT FK_order_carrier
FOREIGN KEY([carrier_id]) REFERENCES [carrier]([id])
ON DELETE NO ACTION
ON UPDATE NO ACTION;
GO


INSERT INTO [DBO].[TRANSPORT_AREA]
           ([TRANSPORT_AREA_TEXT])
     VALUES
           ('ТРАНСПОРТНА ЗОНА')

WHILE (SELECT MAX([TRANSPORT_AREA_ID]) FROM [NOVA_POSHTA].[DBO].[TRANSPORT_AREA]) < 100
BEGIN  
    INSERT INTO [DBO].[TRANSPORT_AREA]
           ([TRANSPORT_AREA_TEXT])
     VALUES
           (CONCAT('ТРАНСПОРТНА ЗОНА', (SELECT MAX ([TRANSPORT_AREA_ID]) FROM [NOVA_POSHTA].[DBO].[TRANSPORT_AREA])))
END;
go


INSERT INTO [city_region]
			([id],
			 [city_region]
			 )
VALUES
			(1,'Львівська'),
			(2,'Київська'),
			(3,'Дніпропетровська'),
			(4,'Харківська'),
			(5,'Одеська'),
			(6,'Рівенська'),
			(7,'Черкаська'),
			(8,'Чернівецька'),
			(9,'Чернігівська'),
			(10,'Сумська');
GO

INSERT INTO [city_district]
			([id],
			 [city_district]
			 )
VALUES
			(1,'Залізничний'),
			(2,'Дарницький'),
			(3,'Шевченківський'),
			(4,'Слобідський'),
			(5,'Приморський'),
			(6,'Городоцький'),
			(7,'Стрийський'),
			(8,'Дубенський'),
			(9,'Південний'),
			(10,'Уманський');
GO

INSERT INTO [country]
			([id],
			 [country]
			 )
VALUES
			(1,'Україна'),
			(2,'Білорусія'),
			(3,'Молдова'),
			(4,'Литва'),
			(5,'Латвія'),
			(6,'Естонія'),
			(7,'Польща'),
			(8,'Словаччина'),
			(9,'Угорщина'),
			(10,'Болгарія');
GO

INSERT INTO [city]
			([city_id],
			 [city_text],
			 [city_region_id],
			 [city_district_id],
			 [country_id],
			 [transport_area_id],
			 [city_before_decomunization]
			 )
VALUES
			(1,'Львів',1,1,1,1,NULL),
			(2,'Київ',2,2,1,2,NULL),
			(3,'Дніпро',3,3,1,3,'Дніпропетровськ'),
			(4,'Харків',4,4,1,4,NULL),
			(5,'Одеса',5,5,1,5,NULL),
			(6,'Городок',1,6,1,1,NULL),
			(7,'Стрий',1,7,1,1,NULL),
			(8,'Дубно',6,8,1,6,NULL),
			(9,'Черкаси',7,9,1,7,NULL),
			(10,'Умань',7,10,1,7,NULL);

GO


insert into employee(id,[name],surname,patronymic,phone,[address],date_of_hire,salary,date_of_birth) values
(1,'Igor','Bas','Olegovich','0734945682','vrub15','2018-01-01','3200.00','1999-07-08'),
(2,'Oleg','Olegich','Olegovich','0504945682','vodokanal 15','2012-12-12','4200.00','1999-06-24'),
(3,'Igor','Vasil','Vasilovich','0735555682','pasichna 111','2013-06-01','3200.00','1993-02-18'),
(4,'Yuriy','Kalutivski','Igorovich','0734946662','batalna 125','2013-05-01','4500.00','1991-01-01'),
(5,'Igor','Baalans','Petrovich','0734943332','kerchenska 17a','2013-01-01','5200.00','1992-12-12'),
(6,'Denis','Mart','Olegovich','0674943332','Stepana Banderu 1','2015-01-01','3200.00','1995-11-11'),
(7,'Oleksiy','Bulko','Petrovich','0974665682','Evreiska 32','2018-05-01','4200.00','1993-02-02'),
(8,'Nazar','Stasiv','Petrovich','0504655682','vrub 17','2018-11-01','5050.00','1992-03-02'),
(9,'Orest','Novikov','Igorovich','0637895682','vodokanal 16','2016-05-21','5200.00','1997-07-28'),
(10,'Vlad','Kosmos','Olegovich','0508845682','zelena 146','2017-11-01','6200.00','1998-06-08');
GO

insert into carrier(id,mark_of_car,name_of_driver,surname_of_driver,id_receiver_sender,[from],[to],departure_date,date_of_receiving)
values
(1,'volvo','Vasil','Ptushkin',1,'Kiev','Lviv','2018-01-07','2018-01-09'),
(2,'shkoda','Vova','Babl',2,'Paris','Lviv','2017-11-07','2017-11-11'),
(3,'shkoda','Viktor','valkovich',3,'Kasandra','Lviv','2017-12-07','2018-12-10'),
(4,'volvo','Denis','Vidanskii',4,'Kiev','Lviv','2018-03-12','2018-03-13'),
(5,'mercedes','Edem','Bydy',5,'Moscow','Lviv','2018-04-07','2018-04-09'),
(6,'mercedes','Vasil','Pazza',6,'Odessa','Lviv','2016-07-08','2016-07-09'),
(7,'toyota','Vova','Prosto',7,'Paris','Lviv','2016-10-07','2016-10-10'),
(8,'toyota','Igor','Sokilov',8,'Budapesht','Lviv','2018-03-15','2018-03-17'),
(9,'nissan','Oleg','Jdunov',9,'Chanioti','Lviv','2017-12-31','2018-01-03'),
(10,'nissan','Nazar','Blashkov',10,'Kiev','Lviv','2017-12-15','2018-12-16');
GO

INSERT INTO warehouse(id, city_id, number_within_city, street_name, street_number, 
                      postal_code, location_cordinate, telephone_number, email,
					  open_date, close_date, head_id, warehouse_type, weight_limitation,
					  office_area, workstation_number, monthly_rent)
VALUES
					 (1,1,1,'Городоцька','356',
					 '89000','35.0204,56.0424','+380975683995','nova_poshta_1@np.ua',
					 '20080211',NULL,NULL,'Вантажне',NULL,
					  24,3,NULL),

					  (2,1,2,'Пластова','7',
					 '89002','35.0704,56.0824','+380975463666','nova_poshta_2@np.ua',
					 '20090711',NULL,NULL,'Поштове',35,
					  30,2,NULL),

					  (3,2,1,'Вишнева','11',
					 '45050','36.0204,51.0424','+380975683965','nova_poshta_3@np.ua',
					 '20080711',NULL,NULL,'Вантажне',NULL,
					  15,3,12000),

					  (4,3,5,'Перемоги','141',
					 '34400','32.0204,56.0224','+380975683925',NULL,
					 '20100911',NULL,NULL,'Вантажне',NULL,
					  24,3,NULL),

					  (5,4,4,'Шевченка','23',
					 '53020','31.0204,55.0424','+380975663995','nova_poshta_6@np.ua',
					 '20150221',NULL,NULL,'Поштове',20,
					  29,1,5000),

					  (6,5,1,'Лесі Українки','1',
					 '89430','37.0204,56.0424','+380975686995','nova_poshta_7@np.ua',
					 '20120211',NULL,NULL,'Вантажне',NULL,
					  20,4,NULL),

					  (7,6,2,'Липова','15',
					 '19400','36.4204,55.0324','+380978683995','nova_poshta_8@np.ua',
					 '20081211',NULL,NULL,'Поштове',15,
					  15,2,7500),

					  (8,7,1,'Торецька','2',
					 '23453','35.1244,54.0324','+380973464222','nova_poshta_9@np.ua',
					 '20130611',NULL,NULL,'Вантажне',NULL,
					  30,3,NULL),

					  (9,8,1,'Вахули','45',
					 '45221','34.0504,52.0624','+380975684995',NULL,
					 '20120614',NULL,NULL,'Поштове',40,
					  11,2,NULL),

					  (10,9,3,'Різніченка','34',
					 '64221','36.0304,54.0524','+380975687995',NULL,
					 '20080211',NULL,NULL,'Вантажне',NULL,
					  24,5,11000);
GO

INSERT INTO schedule(warehouse_id,[weekday],schedule_type,open_time,close_time)
VALUES
					(1,1,'Видача','08:00','17:00'),
					(1,2,'Видача','08:00','17:00'),
					(1,3,'Видача','08:00','17:00'),
					(1,4,'Видача','08:00','17:00'),
					(1,5,'Видача','08:00','17:00'),
					(1,1,'Прийом','10:00','18:00'),
					(1,2,'Прийом','10:00','18:00'),
					(1,3,'Прийом','10:00','18:00'),
					(1,4,'Прийом','10:00','18:00'),
					(1,5,'Прийом','10:00','18:00'),

					(2,1,'Видача','08:00','17:00'),
					(2,2,'Видача','08:00','17:00'),
					(2,3,'Видача','08:00','17:00'),
					(2,4,'Видача','08:00','17:00'),
					(2,5,'Видача','08:00','17:00'),
					(2,1,'Прийом','10:00','18:00'),
					(2,2,'Прийом','10:00','18:00'),
					(2,3,'Прийом','10:00','18:00'),
					(2,4,'Прийом','10:00','18:00'),
					(2,5,'Прийом','10:00','18:00'),

					(3,1,'Видача','09:00','19:00'),
					(3,2,'Видача','09:00','19:00'),
					(3,3,'Видача','09:00','19:00'),
					(3,4,'Видача','09:00','19:00'),
					(3,5,'Видача','09:00','19:00'),
					(3,1,'Прийом','10:00','20:00'),
					(3,2,'Прийом','10:00','20:00'),
					(3,3,'Прийом','10:00','20:00'),
					(3,4,'Прийом','10:00','20:00'),
					(3,5,'Прийом','10:00','20:00'),

					(4,1,'Видача','08:00','17:00'),
					(4,2,'Видача','08:00','17:00'),
					(4,3,'Видача','08:00','17:00'),
					(4,4,'Видача','08:00','17:00'),
					(4,5,'Видача','08:00','17:00'),
					(4,1,'Прийом','10:00','18:00'),
					(4,2,'Прийом','10:00','18:00'),
					(4,3,'Прийом','10:00','18:00'),
					(4,4,'Прийом','10:00','18:00'),
					(4,5,'Прийом','10:00','18:00'),

					(5,1,'Видача','08:00','17:00'),
					(5,2,'Видача','08:00','17:00'),
					(5,3,'Видача','08:00','17:00'),
					(5,4,'Видача','08:00','17:00'),
					(5,5,'Видача','15:00','21:00'),
					(5,1,'Прийом','10:00','18:00'),
					(5,2,'Прийом','10:00','18:00'),
					(5,3,'Прийом','10:00','18:00'),
					(5,4,'Прийом','10:00','18:00'),
					(5,5,'Прийом','10:00','18:00'),

					(6,1,'Видача','08:00','17:00'),
					(6,2,'Видача','08:00','17:00'),
					(6,3,'Видача','08:00','17:00'),
					(6,4,'Видача','08:00','17:00'),
					(6,5,'Видача','08:00','17:00'),
					(6,1,'Прийом','10:00','18:00'),
					(6,2,'Прийом','10:00','18:00'),
					(6,3,'Прийом','12:00','20:00'),
					(6,4,'Прийом','10:00','18:00'),
					(6,5,'Прийом','10:00','18:00'),

					(7,1,'Видача','08:00','17:00'),
					(7,2,'Видача','08:00','17:00'),
					(7,3,'Видача','08:00','17:00'),
					(7,4,'Видача','08:00','17:00'),
					(7,5,'Видача','08:00','17:00'),
					(7,1,'Прийом','10:00','18:00'),
					(7,2,'Прийом','10:00','18:00'),
					(7,3,'Прийом','10:00','18:00'),
					(7,4,'Прийом','10:00','18:00'),
					(7,5,'Прийом','10:00','18:00'),

					(8,1,'Видача','08:00','17:00'),
					(8,2,'Видача','08:00','17:00'),
					(8,3,'Видача','08:00','17:00'),
					(8,4,'Видача','08:00','17:00'),
					(8,5,'Видача','08:00','17:00'),
					(8,1,'Прийом','10:00','18:00'),
					(8,2,'Прийом','10:00','18:00'),
					(8,3,'Прийом','10:00','18:00'),
					(8,4,'Прийом','10:00','18:00'),
					(8,5,'Прийом','10:00','18:00'),

					(9,1,'Видача','08:00','17:00'),
					(9,2,'Видача','08:00','17:00'),
					(9,3,'Видача','08:00','17:00'),
					(9,4,'Видача','08:00','17:00'),
					(9,5,'Видача','08:00','17:00'),
					(9,1,'Прийом','10:00','18:00'),
					(9,2,'Прийом','11:00','18:00'),
					(9,3,'Прийом','10:00','18:00'),
					(9,4,'Прийом','10:00','18:00'),
					(9,5,'Прийом','10:00','18:00'),

					(10,1,'Видача','08:00','17:00'),
					(10,2,'Видача','08:00','17:00'),
					(10,3,'Видача','08:00','17:00'),
					(10,4,'Видача','08:00','17:00'),
					(10,5,'Видача','08:00','17:00'),
					(10,1,'Прийом','10:00','18:00'),
					(10,2,'Прийом','10:00','18:00'),
					(10,3,'Прийом','10:00','18:00'),
					(10,4,'Прийом','10:00','18:00'),
					(10,5,'Прийом','10:00','18:00');
GO

INSERT INTO client(id, city_id, street_name,street_number,
                   client_type, [name], last_name, middle_name,
				   inn_code, passport,
				   ownership_type, edrpou_code,
				   telephone_number, email)
VALUES
				  (1,1,'Шевченка','12',
				  'фізособа','Ігор','Коропецький','Петрович',
				  '2422441134','КС000111',
				  NULL,NULL,
				  '+380975612244','ikor42@meta.ua'),

				  (2,3,'Франка','1',
				  'фізособа','Петро','Стасів','Олегович',
				  '5222441134','КМ000111',
				  NULL,NULL,
				  '+380971112344',NULL),

				  (3,4,'Стуса','2',
				  'фізособа','Андрій','Андропов','Тарасович',
				  '2662241134','КЕ000111',
				  NULL,NULL,
				  '+380971452244','ANDR@met.ua'),

				  (4,2,'Перемоги','126',
				  'фізособа','Таміла','Вишневська','Юріївна',
				  '2222341134','КГ000111',
				  NULL,NULL,
				  '+380976712244',NULL),

				  (5,5,'Вашингтона','192',
				  'фізособа','Рожден','Київський','Васильович',
				  '7222441134','ТС000111',
				  NULL,NULL,
				  '+380971752244','rozhden@i.ua'),

				  (6,7,'Броварська','592',
				  'юрособа','Пролісок',NULL,NULL,
				  NULL,NULL,
				  'ТОВ','22331112',
				  '+380971652244','prolisok@gmail.com'),

				  (7,6,'Вітрова','59',
				  'юрособа','Дзвін',NULL,NULL,
				  NULL,NULL,
				  'ТОВ','56336145',
				  '+380736652244','dzvin@gmail.com'),

				  (8,10,'Проїзна','145',
				  'юрособа','Старт',NULL,NULL,
				  NULL,NULL,
				  'ПрАТ','72311167',
				  '+380961452744','start@gmail.com'),

				  (9,8,'Довбуша','56',
				  'юрособа','Ламела',NULL,NULL,
				  NULL,NULL,
				  'ТОВ','54331143',
				  '+380671352244','lamela@gmail.com'),

				  (10,9,'Потапова','92',
				  'юрособа','Факро',NULL,NULL,
				  NULL,NULL,
				  'ПрАТ','92351189',
				  '+380501552944','fakro@gmail.com');
GO

INSERT INTO bank (id, edrpou_code, full_name)
VALUES
                 ('305299','14360570','АТ КБ «ПРИВАТБАНК»'),
				 ('300528','21685166','АТ «ОТП БАНК»'),
				 ('300335','14305909','«АВАЛь БАНК»'),
				 ('325365','09807862','ПАТ "КРЕДОБАНК"');
 GO

 INSERT INTO bank_account(client_id, bank_id, number)
 VALUES
                         (6,'305299','2245327643561'),
						 (7,'300528','2235637356432'),
						 (8,'305299','2245636734645'),
						 (9,'300335','225675434565'),
						 (10,'325365','2246434565444');
GO

INSERT INTO [DBO].[SHIPPING_TYPES]
           ([SHIPPING_TYPE_TEXT])
     VALUES
           ('АВТОТРАНСПОРТ')

WHILE (SELECT MAX([SHIPPING_TYPE_ID]) FROM [NOVA_POSHTA].[DBO].[SHIPPING_TYPES]) < 100
BEGIN  
    INSERT INTO [DBO].[SHIPPING_TYPES]
           ([SHIPPING_TYPE_TEXT])
     VALUES
           (CONCAT('АВТОТРАНСПОРТ', (SELECT MAX ([SHIPPING_TYPE_ID]) FROM [NOVA_POSHTA].[DBO].[SHIPPING_TYPES])))
END;
go
---------------
INSERT INTO [NOVA_POSHTA].[DBO].[PACKING_TYPES]
           ([PACKING_TEXT])
     VALUES
           ('КОРОБКА 20Х20Х20')
WHILE (SELECT MAX([PACKING_ID]) FROM [NOVA_POSHTA].[DBO].[PACKING_TYPES]) < 100
BEGIN  
    INSERT INTO [NOVA_POSHTA].[DBO].[PACKING_TYPES]
           ([PACKING_TEXT])
     VALUES
           (CONCAT('КОРОБКА 20Х20Х20', (SELECT MAX ([PACKING_ID]) FROM [NOVA_POSHTA].[DBO].[PACKING_TYPES])))
END;
GO

INSERT INTO [NOVA_POSHTA].[DBO].[DEADLINE_TYPE]
           ([DEADLINE_TYPE_TEXT]
		   ,[DEADLINE_TYPE_DELAY])
     VALUES
           ('ДО 2 ДНІВ'
		   ,1)
WHILE (SELECT MAX([DEADLINE_TYPE_ID]) FROM [NOVA_POSHTA].[DBO].[DEADLINE_TYPE]) < 10
BEGIN  
    INSERT INTO [NOVA_POSHTA].[DBO].[DEADLINE_TYPE]
           ([DEADLINE_TYPE_TEXT]
		   ,[DEADLINE_TYPE_DELAY])
     VALUES
           (CONCAT('ДО',' ',(SELECT MAX ([DEADLINE_TYPE_ID]) FROM [NOVA_POSHTA].[DBO].[DEADLINE_TYPE]),' ','ДНІВ')
		   ,1)
END;
GO

INSERT INTO [NOVA_POSHTA].[DBO].[CALCULATION_SCHEME]
           ([CALCULATION_SCHEME_TEXT]
           ,[CALCULATION_SCHEME_PRICE]
           ,[CALCULATION_SCHEME_DISCOUNT_ABS]
           ,[CALCULATION_SCHEME_DISCOUNT_PERC]
           ,[CALCULATION_SCHEME_TAX])
     VALUES
           ('СТАНДАРТ'
		   ,30
		   ,0
		   ,0
		   ,20)
WHILE (SELECT MAX([CALCULATION_SCHEME_ID]) FROM [NOVA_POSHTA].[DBO].[CALCULATION_SCHEME]) < 10
BEGIN  
    INSERT INTO [NOVA_POSHTA].[DBO].[CALCULATION_SCHEME]
           ([CALCULATION_SCHEME_TEXT]
           ,[CALCULATION_SCHEME_PRICE]
           ,[CALCULATION_SCHEME_DISCOUNT_ABS]
           ,[CALCULATION_SCHEME_DISCOUNT_PERC]
           ,[CALCULATION_SCHEME_TAX])
     VALUES
           (CONCAT('СТАНДАРТ',(SELECT MAX([CALCULATION_SCHEME_ID]) FROM [NOVA_POSHTA].[DBO].[CALCULATION_SCHEME]))
		   ,30
		   ,0
		   ,0
		   ,20)
END;
GO

INSERT INTO [DBO].[SHIPPING_POINT]
           ([SHIPPING_POINT_TEXT])
     VALUES
           ('СКЛАД-СКЛАД')
WHILE (SELECT MAX([SHIPPING_POINT_ID]) FROM [NOVA_POSHTA].[DBO].[SHIPPING_POINT]) < 10
BEGIN  
INSERT INTO [DBO].[SHIPPING_POINT]
           ([SHIPPING_POINT_TEXT])
     VALUES
           (CONCAT('СКЛАД-СКЛАД', (SELECT MAX([SHIPPING_POINT_ID]) FROM [NOVA_POSHTA].[DBO].[SHIPPING_POINT])))
END;
GO

INSERT INTO [dbo].[customer_type]
           ([customer_type_text])
     VALUES
           ('ФОП')
WHILE (SELECT MAX([customer_type_id]) FROM [NOVA_POSHTA].[dbo].[customer_type]) < 10
BEGIN  
INSERT INTO [dbo].[customer_type]
           ([customer_type_text])
     VALUES
           (CONCAT('ФОП', (SELECT MAX([customer_type_id]) FROM [NOVA_POSHTA].[dbo].[customer_type])))
END;
GO

INSERT INTO [NOVA_POSHTA].[DBO].[SHIPMENT_TYPE]
           ([SHIPMENT_TYPE_TEXT]
           ,[SHIPPING_TYPE_ID]
           ,[BASE_UNIT]
           ,[BASE_CURRENCY]
           ,[PACKING_ID]
           ,[SPEC_PACKING]
           ,[DEADLINE_TYPE_ID]
           ,[CALCULATION_SCHEME_ID]
           ,[TRANSPORT_AREA_ID]
           ,[PRIORITY_GRADE]
           ,[SHIPPING_POINT_ID]
           ,[CUSTOMER_TYPE_ID]
           ,[RELEVANT_FOR_LOYALTY]
           ,[PREPAYMENT])
     VALUES
           ('УКРАЇНА(ВАНТАЖІВКА <20Т)'
           ,1
           ,'КГ'
           ,'ГРН'
           ,1
           ,''
           ,1
           ,2
           ,1
           ,''
           ,1
           ,1
           ,''
           ,1)
WHILE (SELECT MAX([SHIPMENT_TYPE_ID]) FROM [NOVA_POSHTA].[DBO].[SHIPMENT_TYPE]) < 100
BEGIN  
    INSERT INTO [NOVA_POSHTA].[DBO].[SHIPMENT_TYPE]
           ([SHIPMENT_TYPE_TEXT]
           ,[SHIPPING_TYPE_ID]
           ,[BASE_UNIT]
           ,[BASE_CURRENCY]
           ,[PACKING_ID]
           ,[SPEC_PACKING]
           ,[DEADLINE_TYPE_ID]
           ,[CALCULATION_SCHEME_ID]
           ,[TRANSPORT_AREA_ID]
           ,[PRIORITY_GRADE]
           ,[SHIPPING_POINT_ID]
           ,[CUSTOMER_TYPE_ID]
           ,[RELEVANT_FOR_LOYALTY]
           ,[PREPAYMENT])
     VALUES
           (CONCAT('УКРАЇНА(ВАНТАЖІВКА <20Т)',(SELECT MAX([SHIPMENT_TYPE_ID]) FROM [NOVA_POSHTA].[DBO].[SHIPMENT_TYPE]))
           ,(SELECT MAX([SHIPPING_TYPE_ID]) FROM [NOVA_POSHTA].[DBO].[SHIPPING_TYPES])
           ,'КГ'
           ,'ГРН'
           ,(SELECT MAX([PACKING_ID]) FROM [NOVA_POSHTA].[DBO].[PACKING_TYPES])
           ,''
           ,(SELECT MAX([DEADLINE_TYPE_ID]) FROM [NOVA_POSHTA].[DBO].[DEADLINE_TYPE])
           ,(SELECT MAX([CALCULATION_SCHEME_ID]) FROM [NOVA_POSHTA].[DBO].[CALCULATION_SCHEME])
           ,(SELECT MAX([TRANSPORT_AREA_ID]) FROM [NOVA_POSHTA].[DBO].[TRANSPORT_AREA])
           ,''
           ,(SELECT MAX([SHIPPING_POINT_ID]) FROM [NOVA_POSHTA].[DBO].[SHIPPING_POINT])
           ,(SELECT MAX([CUSTOMER_TYPE_ID]) FROM [NOVA_POSHTA].[DBO].[CUSTOMER_TYPE])
           ,''
           ,1)
END;
GO

INSERT INTO [order]
					([id],
					[shipment_type_id],
					[tracking_number] ,
					[departure_warehouse_id] ,
					[destination_warehouse_id] ,
					[sender_id] ,
					[reciever_id] ,
					[payer_id] ,
					[took_order_employee_id] ,
					[gave_out_order_employee_id] ,
					[carrier_id] ,
					[receiving_date] ,
					[arriving_date] ,
					[gave_out_date] ,
					[order_cost] ,
					[weight] ,
					[is_reversed] )
VALUES
					(1,2,123456789,3,6,1,2,1,1,4,2,'2012-02-15','2012-02-19','2012-02-20',50.00,3.5,'false'),
					(2,4,234567891,4,7,2,3,3,1,4,2,'2012-03-17','2012-03-18','2012-03-20',50.00,3.5,'false'),
					(3,2,345678912,5,8,3,4,3,1,4,2,'2014-06-04','2014-06-19','2014-06-20',50.00,3.5,'false'),
					(4,2,456789123,6,9,5,6,6,1,4,2,'2015-02-21','2015-02-24','2015-02-24',50.00,3.5,'false'),
					(5,3,567891234,7,1,6,7,6,1,4,2,'2015-03-11','2015-03-14','2015-03-14',60.00,4.5,'false'),
					(6,7,678912345,8,2,7,8,8,1,4,2,'2015-04-14','2015-04-18','2015-04-20',50.00,3.5,'false'),
					(7,2,789123456,9,3,8,9,9,1,4,2,'2015-06-14','2015-06-18','2015-06-20',50.00,3.5,'false'),
					(8,2,891234567,1,4,9,1,1,1,4,2,'2015-08-14','2015-08-18','2015-08-18',50.00,3.5,'false'),
					(9,8,912345678,2,5,1,4,4,1,4,2,'2015-09-14','2015-09-18','2015-09-18',70.00,5.5,'false'),
					(10,2,192345678,3,7,1,5,5,1,4,2,'2015-10-14','2015-10-18','2015-10-18',50.00,3.5,'true'),
					(11,3,182345678,4,8,1,6,1,1,4,2,'2015-11-14','2015-11-18','2015-11-18',50.00,3.5,'false'),
					(12,7,172345678,5,9,1,7,1,1,4,2,'2015-12-14','2015-12-18','2015-12-18',40.00,3.5,'false'),
					(13,6,162345678,6,1,1,8,1,1,4,2,'2015-08-14','2015-08-18','2015-08-18',50.00,3.5,'false'),
					(14,2,152345678,7,2,2,4,4,1,4,2,'2015-08-14','2015-08-18','2015-08-18',50.00,3.5,'false'),
					(15,7,142345678,8,3,3,5,5,1,4,2,'2015-08-14','2015-08-18','2015-08-18',60.00,4.5,'false'),
					(16,4,132345678,9,4,3,2,3,1,4,2,'2015-08-14','2015-08-18','2015-08-18',50.00,3.5,'true'),
					(17,4,122345678,1,5,6,8,6,1,4,2,'2015-08-14','2015-08-18','2015-08-18',50.00,3.5,'false'),
					(18,8,871123456,2,6,4,2,4,1,4,2,'2015-08-14','2015-08-18','2015-08-18',60.00,4.5,'false'),
					(19,8,187234567,3,8,1,2,1,1,4,2,'2015-08-14','2015-08-18','2015-08-18',50.00,3.5,'false'),
					(20,2,128734567,4,9,1,2,1,1,4,2,'2015-08-14','2015-08-18','2015-08-18',45.00,2.5,'false');
GO