﻿USE [Nova_Poshta];
GO

DROP TABLE IF EXISTS [order];
DROP TABLE IF EXISTS [city];
DROP TABLE IF EXISTS [city_region];
DROP TABLE IF EXISTS [city_district];
DROP TABLE IF EXISTS [country];
DROP TABLE IF EXISTS [transport_area];
GO


CREATE TABLE [city_region]
			([id] INT NOT NULL PRIMARY KEY,
			 [city_region] VARCHAR(40) NOT NULL DEFAULT ('Львівська'),
			 )
GO

CREATE TABLE [city_district]
			([id] INT NOT NULL PRIMARY KEY,
			 [city_district] VARCHAR(40) NOT NULL
			 )
GO

CREATE TABLE [country]
			([id] INT NOT NULL PRIMARY KEY,
			 [country] VARCHAR(40) NOT NULL DEFAULT ('Україна'),
			 )
GO


CREATE TABLE [city]
			([city_id] INT NOT NULL PRIMARY KEY,
			 [city_text] VARCHAR(40) NOT NULL DEFAULT ('Львів'),
			 [city_region_id] INT NOT NULL  DEFAULT (1),
			 [city_district_id] INT NOT NULL DEFAULT (1),
			 [country_id] INT NOT NULL  DEFAULT (1),
			 [transport_area_id] INT NULL DEFAULT (1),
			 [city_before_decomunization] VARCHAR(40) NULL
			 )
GO

ALTER TABLE [city]
ADD CONSTRAINT FK_city_city_region
FOREIGN KEY([city_region_id]) REFERENCES [city_region]([id])
ON DELETE CASCADE
ON UPDATE CASCADE
GO

ALTER TABLE [city]
ADD CONSTRAINT FK_city_city_district
FOREIGN KEY([city_district_id]) REFERENCES [city_district]([id])
ON DELETE SET DEFAULT
ON UPDATE SET DEFAULT
GO

ALTER TABLE [city]
ADD CONSTRAINT FK_city_country
FOREIGN KEY([country_id]) REFERENCES [country]([id])
ON DELETE CASCADE
ON UPDATE CASCADE
GO

ALTER TABLE [city]
ADD CONSTRAINT FK_city_transport_area
FOREIGN KEY([transport_area_id]) REFERENCES [transport_area]([id])
ON DELETE SET DEFAULT
ON UPDATE SET DEFAULT
GO

CREATE TABLE [order]
			([id] INT NOT NULL PRIMARY KEY,
			 [shipment_type_id] INT NOT NULL,
			 [tracking_number] INT NOT NULL UNIQUE,
			 [departure_warehouse_id] INT NOT NULL,
			 [destination_warehouse_id] INT NOT NULL,
			 [sender_id] INT NOT NULL,
			 [reciever_id] INT NOT NULL,
			 [payer_id] INT NOT NULL,
			 [took_order_employee_id] INT NOT NULL,
			 [gave_out_order_employee_id] INT NULL,
			 [carrier_id] INT NULL,
			 [receiving_date] DATETIME NOT NULL,
			 [arriving_date] DATETIME NOT NULL,
			 [gave_out_date] DATETIME NULL,
			 [order_cost] MONEY NOT NULL DEFAULT (0) CHECK ([order_cost] >= 0),
			 [weight] real NOT NULL DEFAULT (0) CHECK ([weight] >= 0),
			 [total_cost] AS ([order_cost]+ISNULL(0.01*[order_cost]*DATEDIFF(DAY, [arriving_date],[gave_out_date]),0)),
			 [is_reversed] BIT NOT NULL,
			)
GO

ALTER TABLE [order]
ADD CONSTRAINT CK_order_arriving_date
CHECK ([arriving_date] >= [receiving_date])
GO

ALTER TABLE [order]
ADD CONSTRAINT CK_order_gave_out_date
CHECK ([gave_out_date] >= [arriving_date])
GO

ALTER TABLE [order]
ADD CONSTRAINT CK_order_payer
CHECK ([payer_id] in ([sender_id],[reciever_id]) )
GO

ALTER TABLE [order]
ADD CONSTRAINT FK_order_shipment_type
FOREIGN KEY([shipment_type_id]) REFERENCES [shipment_type]([shipment_type_id])
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO

ALTER TABLE [order]
ADD CONSTRAINT FK_order_departure_warehouse
FOREIGN KEY([departure_warehouse_id]) REFERENCES [warehouse]([id])
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO

ALTER TABLE [order]
ADD CONSTRAINT FK_order_destination_warehouse
FOREIGN KEY([destination_warehouse_id]) REFERENCES [warehouse]([id])
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO

ALTER TABLE [order]
ADD CONSTRAINT FK_order_sender
FOREIGN KEY([sender_id]) REFERENCES [client]([id])
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO

ALTER TABLE [order]
ADD CONSTRAINT FK_order_reciever
FOREIGN KEY([reciever_id]) REFERENCES [client]([id])
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO

ALTER TABLE [order]
ADD CONSTRAINT FK_order_payer
FOREIGN KEY([payer_id]) REFERENCES [client]([id])
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO

ALTER TABLE [order]
ADD CONSTRAINT FK_order_took_order_employee
FOREIGN KEY([took_order_employee_id]) REFERENCES [employee]([id])
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO

ALTER TABLE [order]
ADD CONSTRAINT FK_order_gave_out_order_employee
FOREIGN KEY([gave_out_order_employee_id]) REFERENCES [employee]([id])
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO

ALTER TABLE [order]
ADD CONSTRAINT FK_order_carrier
FOREIGN KEY([carrier_id]) REFERENCES [carrier]([id])
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO



/*
CREATE TABLE [transport_area]
			([id] INT NOT NULL PRIMARY KEY,
			 [transport_area] INT NULL
			 )
GO*/