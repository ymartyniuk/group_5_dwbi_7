﻿USE master;
GO

DROP DATABASE IF EXISTS Nova_Poshta;
GO

CREATE DATABASE Nova_Poshta;
GO

USE Nova_Poshta;
GO

DROP TABLE IF EXISTS warehouse;
DROP TABLE IF EXISTS schedule;
DROP TABLE IF EXISTS client;
DROP TABLE IF EXISTS bank_account;
DROP TABLE IF EXISTS bank;
GO

CREATE TABLE warehouse
(id INT NOT NULL PRIMARY KEY,
 city_id INT NOT NULL,
 number_within_city SMALLINT NOT NULL,
 street_name VARCHAR(50) NOT NULL,
 street_number VARCHAR(10) NOT NULL,
 postal_code CHAR(5) NOT NULL,
 location_cordinate CHAR(15) NOT NULL UNIQUE,
 telephone_number CHAR(13) NOT NULL UNIQUE,
 email VARCHAR(50) NULL,
 open_date DATE NOT NULL,
 close_date DATE NULL,
 head_id int NULL,
 warehouse_type VARCHAR(20) NOT NULL,
 weight_limitation SMALLINT NULL,
 office_area NUMERIC(5,2) NOT NULL,
 workstation_number TINYINT NOT NULL,
 monthly_rent NUMERIC (7,2) NULL
);

CREATE TABLE schedule
(warehouse_id INT NOT NULL,
 [weekday] TINYINT NOT NULL,
 schedule_type VARCHAR(20) NOT NULL,
 open_time TIME NOT NULL,
 close_time TIME NOT NULL
 CONSTRAINT PK_schedule PRIMARY KEY(warehouse_id,[weekday],schedule_type)
);

CREATE TABLE client
(id INT NOT NULL PRIMARY KEY,
 city_id INT NOT NULL,
 street_name VARCHAR(50) NOT NULL,
 street_number VARCHAR(10) NOT NULL,
 client_type VARCHAR(20) NOT NULL,
 name VARCHAR(20) NOT NULL,
 last_name VARCHAR(30) NULL,
 middle_name VARCHAR(25) NULL,
 inn_code CHAR(10) NULL,
 passport CHAR(8) NULL,
 ownership_type VARCHAR(20) NULL,
 edrpou_code CHAR(8) NULL,
 telephone_number CHAR(13) NOT NULL UNIQUE,
 email VARCHAR(50) NULL
);

CREATE TABLE bank_account
(client_id INT NOT NULL,
 bank_id CHAR(6) NOT NULL,
 number VARCHAR(13) NOT NULL
 CONSTRAINT PK_bank_account PRIMARY KEY(client_id,bank_id,number)
);

CREATE TABLE bank
(id CHAR(6) NOT NULL PRIMARY KEY,
 edrpou_code CHAR(8) NOT NULL,
 full_name VARCHAR(50) NOT NULL
);

--ВСІ ОБМЕЖЕННЯ ЗОВНІШНІ ОБМЕЖЕННЯ ПИШЕМО ПІСЛЯ ОГОЛОШЕННЯ ВСІХ ТАБЛИЦЬ

ALTER TABLE warehouse
ADD CONSTRAINT UK_warehouse
UNIQUE(city_id,number_within_city);

ALTER TABLE schedule
ADD CONSTRAINT FK_schedule_warehouse
FOREIGN KEY(warehouse_id)
REFERENCES warehouse(id);

ALTER TABLE warehouse
ADD CONSTRAINT FK_warehouse_city
FOREIGN KEY(city_id)
REFERENCES city(city_id);

ALTER TABLE warehouse
ADD CONSTRAINT FK_warehouse_head
FOREIGN KEY(head_id)
REFERENCES employee(id);

ALTER TABLE client
ADD CONSTRAINT FK_client_city
FOREIGN KEY(city_id)
REFERENCES city(city_id);

