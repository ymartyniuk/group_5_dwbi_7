use  Nova_poshta
go

DROP TABLE IF EXISTS employee;
DROP TABLE IF EXISTS carrier;

create table employee(
id int not null primary key,
name varchar(30) not null,
surname varchar(30) not null,
patronymic varchar(30) null,
phone char(10) not null unique,
address varchar(50) not null,
date_of_hire date not null,
date_if_fire date null,
salary decimal(8,2) not null,
date_of_birth date not null
)

create table carrier(
id int not null primary key,
mark_of_car varchar(30) not null,
name_of_driver varchar(30) not null,
surname_of_driver varchar(30) not null,
id_receiver_sender int not null,
[from] varchar(30) not null,
[to] varchar(30) not null,
departure_date date not null,
date_of_receiving date not null
)

insert into employee(id,name,surname,patronymic,phone,address,date_of_hire,salary,date_of_birth)values
(1,'Igor','Bas','Olegovich','0734945682','vrub15','2018-01-01','3200.00','1999-07-08'),
(2,'Oleg','Olegich','Olegovich','0504945682','vodokanal 15','2012-12-12','4200.00','1999-06-24'),
(3,'Igor','Vasil','Vasilovich','0735555682','pasichna 111','2013-06-01','3200.00','1993-02-18'),
(4,'Yuriy','Kalutivski','Igorovich','0734946662','batalna 125','2013-05-01','4500.00','1991-01-01'),
(5,'Igor','Baalans','Petrovich','0734943332','kerchenska 17a','2013-01-01','5200.00','1992-12-12'),
(6,'Denis','Mart','Olegovich','0674943332','Stepana Banderu 1','2015-01-01','3200.00','1995-11-11'),
(7,'Oleksiy','Bulko','Petrovich','0974665682','Evreiska 32','2018-05-01','4200.00','1993-02-02'),
(8,'Nazar','Stasiv','Petrovich','0504655682','vrub 17','2018-11-01','5050.00','1992-03-02'),
(9,'Orest','Novikov','Igorovich','0637895682','vodokanal 16','2016-05-21','5200.00','1997-07-28'),
(10,'Vlad','Kosmos','Olegovich','0508845682','zelena 146','2017-11-01','6200.00','1998-06-08');

insert into carrier(id,mark_of_car,name_of_driver,surname_of_driver,id_receiver_sender,[from],[to],departure_date,date_of_receiving)
values
(1,'volvo','Vasil','Ptushkin',1,'Kiev','Lviv','2018-01-07','2018-01-09'),
(2,'shkoda','Vova','Babl',2,'Paris','Lviv','2017-11-07','2017-11-11'),
(3,'shkoda','Viktor','valkovich',3,'Kasandra','Lviv','2017-12-07','2018-12-10'),
(4,'volvo','Denis','Vidanskii',4,'Kiev','Lviv','2018-03-12','2018-03-13'),
(5,'mercedes','Edem','Bydy',5,'Moscow','Lviv','2018-04-07','2018-04-09'),
(6,'mercedes','Vasil','Pazza',6,'Odessa','Lviv','2016-07-08','2016-07-09'),
(7,'toyota','Vova','Prosto',7,'Paris','Lviv','2016-10-07','2016-10-10'),
(8,'toyota','Igor','Sokilov',8,'Budapesht','Lviv','2018-03-15','2018-03-17'),
(9,'nissan','Oleg','Jdunov',9,'Chanioti','Lviv','2017-12-31','2018-01-03'),
(10,'nissan','Nazar','Blashkov',10,'Kiev','Lviv','2017-12-15','2018-12-16');

