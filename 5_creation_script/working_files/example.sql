﻿USE master;
GO

DROP DATABASE IF EXISTS Nova_Poshta;
GO

CREATE DATABASE Nova_Poshta;
GO

USE Nova_Poshta;
GO

DROP TABLE IF EXISTS warehouse;
DROP TABLE IF EXISTS schedule;
DROP TABLE IF EXISTS client;
DROP TABLE IF EXISTS bank_account;
DROP TABLE IF EXISTS bank;
GO

DROP TABLE IF EXISTS [order];
DROP TABLE IF EXISTS [city];
DROP TABLE IF EXISTS [city_region];
DROP TABLE IF EXISTS [city_district];
DROP TABLE IF EXISTS [country];
DROP TABLE IF EXISTS [transport_area];
GO


DROP TABLE IF EXISTS employee;
DROP TABLE IF EXISTS carrier;

DROP TABLE IF EXISTS customer_type
DROP TABLE IF EXISTS shipping_point
--DROP TABLE IF EXISTS transport_area
DROP TABLE IF EXISTS calculation_scheme
DROP TABLE IF EXISTS deadline_type
DROP TABLE IF EXISTS packing_types
DROP TABLE IF EXISTS packing_types
DROP TABLE IF EXISTS shipping_types
DROP TABLE IF EXISTS shipment_type
go
go

create table transport_area	(
transport_area_id	int identity primary key not null,
transport_area_text	varchar(40) default '�������� ������' not null
)
go

CREATE TABLE [city]
			([city_id] INT NOT NULL PRIMARY KEY,
			 [city_text] VARCHAR(40) NOT NULL DEFAULT ('Львів'),
			 [city_region_id] INT NOT NULL  DEFAULT (1),
			 [city_district_id] INT NOT NULL DEFAULT (1),
			 [country_id] INT NOT NULL  DEFAULT (1),
			 [transport_area_id] INT NULL DEFAULT (1),
			 [city_before_decomunization] VARCHAR(40) NULL
			 )
GO

CREATE TABLE [city_region]
			([id] INT NOT NULL PRIMARY KEY,
			 [city_region] VARCHAR(40) NOT NULL DEFAULT ('Львівська'),
			 )
GO

CREATE TABLE [city_district]
			([id] INT NOT NULL PRIMARY KEY,
			 [city_district] VARCHAR(40) NOT NULL
			 )
GO

CREATE TABLE [country]
			([id] INT NOT NULL PRIMARY KEY,
			 [country] VARCHAR(40) NOT NULL DEFAULT ('Україна'),
			 )
GO

/*
CREATE TABLE [transport_area]
			([id] INT NOT NULL PRIMARY KEY,
			 [transport_area] INT NULL
			 )
GO*/


ALTER TABLE [city]
ADD CONSTRAINT FK_city_city_region
FOREIGN KEY([city_region_id]) REFERENCES [city_region]([id])
ON DELETE CASCADE
ON UPDATE CASCADE
GO

ALTER TABLE [city]
ADD CONSTRAINT FK_city_city_district
FOREIGN KEY([city_district_id]) REFERENCES [city_district]([id])
ON DELETE SET DEFAULT
ON UPDATE SET DEFAULT
GO

ALTER TABLE [city]
ADD CONSTRAINT FK_city_country
FOREIGN KEY([country_id]) REFERENCES [country]([id])
ON DELETE CASCADE
ON UPDATE CASCADE
GO

ALTER TABLE [city]
ADD CONSTRAINT FK_city_transport_area
FOREIGN KEY([transport_area_id]) REFERENCES [transport_area]([transport_area_id])
ON DELETE SET DEFAULT
ON UPDATE SET DEFAULT
GO


create table carrier(
id int not null primary key,
mark_of_car varchar(30) not null,
name_of_driver varchar(30) not null,
surname_of_driver varchar(30) not null,
id_receiver_sender int not null,
[from] varchar(30) not null,
[to] varchar(30) not null,
departure_date date not null,
date_of_receiving date not null
)


create table employee(
id int not null primary key,
name varchar(30) not null,
surname varchar(30) not null,
patronymic varchar(30) null,
phone char(10) not null unique,
address varchar(50) not null,
date_of_hire date not null,
date_if_fire date null,
salary decimal(8,2) not null,
date_of_birth date not null
)

CREATE TABLE warehouse
(id INT NOT NULL PRIMARY KEY,
 city_id INT NOT NULL,
 number_within_city SMALLINT NOT NULL,
 street_name VARCHAR(50) NOT NULL,
 street_number VARCHAR(10) NOT NULL,
 postal_code CHAR(5) NOT NULL,
 location_cordinate CHAR(15) NOT NULL UNIQUE,
 telephone_number CHAR(13) NOT NULL UNIQUE,
 email VARCHAR(50) NULL,
 open_date DATE NOT NULL,
 close_date DATE NULL,
 head_id int NULL,
 warehouse_type VARCHAR(20) NOT NULL,
 weight_limitation SMALLINT NULL,
 office_area NUMERIC(5,2) NOT NULL,
 workstation_number TINYINT NOT NULL,
 monthly_rent NUMERIC (7,2) NULL
);

CREATE TABLE schedule
(warehouse_id INT NOT NULL,
 [weekday] TINYINT NOT NULL,
 schedule_type VARCHAR(20) NOT NULL,
 open_time TIME NOT NULL,
 close_time TIME NOT NULL
 CONSTRAINT PK_schedule PRIMARY KEY(warehouse_id,[weekday],schedule_type)
);

CREATE TABLE client
(id INT NOT NULL PRIMARY KEY,
 city_id INT NOT NULL,
 street_name VARCHAR(50) NOT NULL,
 street_number VARCHAR(10) NOT NULL,
 client_type VARCHAR(20) NOT NULL,
 name VARCHAR(20) NOT NULL,
 last_name VARCHAR(30) NULL,
 middle_name VARCHAR(25) NULL,
 inn_code CHAR(10) NULL,
 passport CHAR(8) NULL,
 ownership_type VARCHAR(20) NULL,
 edrpou_code CHAR(8) NULL,
 telephone_number CHAR(13) NOT NULL UNIQUE,
 email VARCHAR(50) NULL
);

CREATE TABLE bank_account
(client_id INT NOT NULL,
 bank_id CHAR(6) NOT NULL,
 number VARCHAR(13) NOT NULL
 CONSTRAINT PK_bank_account PRIMARY KEY(client_id,bank_id,number)
);

CREATE TABLE bank
(id CHAR(6) NOT NULL PRIMARY KEY,
 edrpou_code CHAR(8) NOT NULL,
 full_name VARCHAR(50) NOT NULL
);

--ВСІ ОБМЕЖЕННЯ ЗОВНІШНІ ОБМЕЖЕННЯ ПИШЕМО ПІСЛЯ ОГОЛОШЕННЯ ВСІХ ТАБЛИЦЬ

ALTER TABLE warehouse
ADD CONSTRAINT UK_warehouse
UNIQUE(city_id,number_within_city);

ALTER TABLE schedule
ADD CONSTRAINT FK_schedule_warehouse
FOREIGN KEY(warehouse_id)
REFERENCES warehouse(id);

ALTER TABLE warehouse
ADD CONSTRAINT FK_warehouse_city
FOREIGN KEY(city_id)
REFERENCES city(city_id);

ALTER TABLE warehouse
ADD CONSTRAINT FK_warehouse_head
FOREIGN KEY(head_id)
REFERENCES employee(id);

ALTER TABLE client
ADD CONSTRAINT FK_client_city
FOREIGN KEY(city_id)
REFERENCES city(city_id);
---USE [Nova_Poshta];
GO

create table customer_type(
customer_type_id int identity primary key not null,
customer_type_text	varchar(40) default '���' not null
)
go

use Nova_Poshta
go
create table shipping_point(
shipping_point_id	int identity primary key not null,
shipping_point_text	varchar(40) default '�����-�����' not null
)
go

use Nova_Poshta
go
create table calculation_scheme	(
calculation_scheme_id	int identity primary key not null,
calculation_scheme_text	varchar(40) not null,
calculation_scheme_price decimal(13,5) not null,
calculation_scheme_discount_abs	decimal(13,5),
calculation_scheme_discount_perc decimal(13,5),
calculation_scheme_tax decimal (13,5) not null
)
go

use Nova_Poshta
go
create table deadline_type(
deadline_type_id int identity primary key not null,
deadline_type_text	varchar(40) not null,
deadline_type_delay	int default 0 check(deadline_type_delay>=0) not null
)
go

use Nova_Poshta
go
create table packing_types(
packing_id int identity primary key not null,
packing_text varchar(40) default '���������' not null,
packing_material varchar(40)
)
go

use Nova_Poshta
go
create table shipping_types	(
shipping_type_id int identity primary key not null,
shipping_type_text	varchar(40) default '�������������' not null
)
go

use Nova_Poshta
go
create table shipment_type(
shipment_type_id int identity primary key not null,
shipment_type_text	varchar(40),
shipping_type_id int foreign key references shipping_types(shipping_type_id) not null,
base_unit varchar(40) default '��',
base_currency varchar(40) default '���',
packing_id int foreign key references packing_types(packing_id) not null,
spec_packing varchar(40) default '��� ����������� ���������',
deadline_type_id int foreign key references deadline_type(deadline_type_id) not null,
calculation_scheme_id int foreign key references calculation_scheme(calculation_scheme_id) not null,
transport_area_id int foreign key references transport_area(transport_area_id) not null,
priority_grade	tinyint default 0,
shipping_point_id int foreign key references shipping_point(shipping_point_id) not null,
customer_type_id int foreign key references customer_type(customer_type_id) not null,
relevant_for_loyalty tinyint,
prepayment	tinyint not null
)
go


CREATE TABLE [order]
			([id] INT NOT NULL PRIMARY KEY,
			 [shipment_type_id] INT NOT NULL,
			 [tracking_number] INT NOT NULL UNIQUE,
			 [departure_warehouse_id] INT NOT NULL,
			 [destination_warehouse_id] INT NOT NULL,
			 [sender_id] INT NOT NULL,
			 [reciever_id] INT NOT NULL,
			 [payer_id] INT NOT NULL ,
			 [took_order_employee_id] INT NOT NULL,
			 [gave_out_order_employee_id] INT NULL,
			 [carrier_id] INT NULL,
			 [receiving_date] DATETIME NOT NULL,
			 [arriving_date] DATETIME NOT NULL,
			 [gave_out_date] DATETIME NULL,
			 [order_cost] MONEY NOT NULL DEFAULT (0) CHECK ([order_cost] >= 0),
			 [weight] real NOT NULL DEFAULT (0) CHECK ([weight] >= 0),
			 [total_cost] AS ([order_cost]+ISNULL(0.01*[order_cost]*DATEDIFF(DAY, [arriving_date],[gave_out_date]),0)),
			 [is_reversed] BIT NOT NULL,
			)
GO

ALTER TABLE [order]
ADD CONSTRAINT CK_order_arriving_date
CHECK ([arriving_date] >= [receiving_date])
GO

ALTER TABLE [order]
ADD CONSTRAINT CK_order_gave_out_date
CHECK ([gave_out_date] >= [arriving_date])
GO

ALTER TABLE [order]
ADD CONSTRAINT CK_order_payer
CHECK ([payer_id] in ([sender_id],[reciever_id]) )
GO

ALTER TABLE [order]
ADD CONSTRAINT FK_order_shipment_type
FOREIGN KEY([shipment_type_id]) REFERENCES [shipment_type]([shipment_type_id])
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO

ALTER TABLE [order]
ADD CONSTRAINT FK_order_departure_warehouse
FOREIGN KEY([departure_warehouse_id]) REFERENCES [warehouse]([id])
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO

ALTER TABLE [order]
ADD CONSTRAINT FK_order_destination_warehouse
FOREIGN KEY([destination_warehouse_id]) REFERENCES [warehouse]([id])
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO

ALTER TABLE [order]
ADD CONSTRAINT FK_order_sender
FOREIGN KEY([sender_id]) REFERENCES [client]([id])
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO

ALTER TABLE [order]
ADD CONSTRAINT FK_order_reciever
FOREIGN KEY([reciever_id]) REFERENCES [client]([id])
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO

ALTER TABLE [order]
ADD CONSTRAINT FK_order_payer
FOREIGN KEY([payer_id]) REFERENCES [client]([id])
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO

ALTER TABLE [order]
ADD CONSTRAINT FK_order_took_order_employee
FOREIGN KEY([took_order_employee_id]) REFERENCES [employee]([id])
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO

ALTER TABLE [order]
ADD CONSTRAINT FK_order_gave_out_order_employee
FOREIGN KEY([gave_out_order_employee_id]) REFERENCES [employee]([id])
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO

ALTER TABLE [order]
ADD CONSTRAINT FK_order_carrier
FOREIGN KEY([carrier_id]) REFERENCES [carrier]([id])
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO

