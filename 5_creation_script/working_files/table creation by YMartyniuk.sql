--table creation by YMartyniuk 22/07/2018
use Nova_Poshta
go
DROP TABLE IF EXISTS customer_type
DROP TABLE IF EXISTS shipping_point
--DROP TABLE IF EXISTS transport_area
DROP TABLE IF EXISTS calculation_scheme
DROP TABLE IF EXISTS deadline_type
DROP TABLE IF EXISTS packing_types
DROP TABLE IF EXISTS packing_types
DROP TABLE IF EXISTS shipping_types
DROP TABLE IF EXISTS shipment_type
go
go
create table customer_type(
customer_type_id int identity primary key not null,
customer_type_text	varchar(40) default '���' not null
)
go

use Nova_Poshta
go
create table shipping_point(
shipping_point_id	int identity primary key not null,
shipping_point_text	varchar(40) default '�����-�����' not null
)
go

--use Nova_Poshta
--go
--create table transport_area	(
--transport_area_id	int identity primary key not null,
--transport_area_text	varchar(40) default '�������� ������' not null
--)
--go

use Nova_Poshta
go
create table calculation_scheme	(
calculation_scheme_id	int identity primary key not null,
calculation_scheme_text	varchar(40) not null,
calculation_scheme_price decimal(13,5) not null,
calculation_scheme_discount_abs	decimal(13,5),
calculation_scheme_discount_perc decimal(13,5),
calculation_scheme_tax decimal (13,5) not null
)
go

use Nova_Poshta
go
create table deadline_type(
deadline_type_id int identity primary key not null,
deadline_type_text	varchar(40) not null,
deadline_type_delay	int default 0 check(deadline_type_delay>=0) not null
)
go

use Nova_Poshta
go
create table packing_types(
packing_id int identity primary key not null,
packing_text varchar(40) default '���������' not null,
packing_material varchar(40)
)
go

use Nova_Poshta
go
create table shipping_types	(
shipping_type_id int identity primary key not null,
shipping_type_text	varchar(40) default '�������������' not null
)
go

use Nova_Poshta
go
create table shipment_type(
shipment_type_id int identity primary key not null,
shipment_type_text	varchar(40),
shipping_type_id int foreign key references shipping_types(shipping_type_id) not null,
base_unit varchar(40) default '��',
base_currency varchar(40) default '���',
packing_id int foreign key references packing_types(packing_id) not null,
spec_packing varchar(40) default '��� ����������� ���������',
deadline_type_id int foreign key references deadline_type(deadline_type_id) not null,
calculation_scheme_id int foreign key references calculation_scheme(calculation_scheme_id) not null,
transport_area_id int foreign key references transport_area(transport_area_id) not null,
priority_grade	tinyint default 0,
shipping_point_id int foreign key references shipping_point(shipping_point_id) not null,
customer_type_id int foreign key references customer_type(customer_type_id) not null,
relevant_for_loyalty tinyint,
prepayment	tinyint not null
)
go